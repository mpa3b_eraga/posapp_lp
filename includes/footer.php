<?php
/**
 * Created by PhpStorm.
 * User: mpa3b
 * Date: 28.04.2016
 * Time: 10:12
 */
?>

<footer id="footer" class="mdl-color--primary mdl-color-text--color-1" style="position:relative; z-index:100;">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <i class="material-icons mdl-color-text--accent">&#xE0BE;</i>
                <span>
                    <a href="mailto:support@posapp.me" class="mdl-color-text--color-2">support@posapp.me</a>
                </span>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 align-right">
                <i class="material-icons mdl-color-text--accent">&#xE0B0;</i>
                <span>
                    <a href="tel:79876543210" class="mdl-color-text--color-2">+7 987 654 32 10</a>
                </span>
            </div>
        </div>
    </div>
</footer>

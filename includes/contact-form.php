<?php
/**
 * Created by PhpStorm.
 * User: mpa3b
 * Date: 28.04.2016
 * Time: 10:08
 */
?>

<section id="contact" class="mdl-color--color-2 mdl-color-text--color-2 subsection">

    <script src='https://www.google.com/recaptcha/api.js'></script>

    <div class="container">
        <div class="row">
            <div
                class="col-xs-6 col-sm-6 col-md-6 col-lg-6 align-left margin-bottom-2 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
                <h4>Заявка на участие в тестировании</h4>

                <form action="/php/send-request.php" id="contact-form" method="post">

                    <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" id="contact-email" type="email" name="contact-email"/>
                        <label class="mdl-textfield__label" for="contact-email">Ваш E-mail</label>
                    </div>

                    <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" id="contact-company" type="text" name="contact-company"/>
                        <label class="mdl-textfield__label" for="contact-company">Ваша компания</label>
                    </div>

                    <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" id="contact-branch" type="text" name="contact-branch"/>
                        <label class="mdl-textfield__label" for="contact-branch">Сфера деятельности</label>
                    </div>

                    <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" id="contact-name" type="text" name="contact-name"/>
                        <label class="mdl-textfield__label" for="contact-name">Ваше имя</label>
                    </div>

                    <div class="mdl-textfield mdl-js-textfield">
                        <input class="mdl-textfield__input" id="contact-phone" type="text" name="contact-phone"/>
                        <label class="mdl-textfield__label" for="contact-phone">Ваш телефон</label>
                    </div>

                    <div class="mdl-textfield">
                        <div class="g-recaptcha" data-sitekey="6Ld1qR0TAAAAAKIEUWU3iFExKsBN_TnULJVuPnNa"></div>
                    </div>

                    <button id="request-send-btn"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent mdl-js-ripple-effect "
                            type="submit" name="action">Хочу участвовать!
                    </button>

                </form>

                <div id="message" class="mdl-color-text--color-1">
                    <div id="alert"></div>
                </div>

                <!-- Message container -->

            </div>

        </div>
    </div>

    <div class="container">
        <div class="row">

            <div
                class="col-xs-6 col-sm-6 col-md-6 col-lg-6 align-left margin-bottom-2 col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3">
                <div class="ya-share2 align-center"
                     data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter" data-image="/logo.png"></div>
            </div>

        </div>
    </div>

</section>
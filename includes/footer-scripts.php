<?php
/**
 * Created by PhpStorm.
 * User: mpa3b
 * Date: 28.04.2016
 * Time: 10:13
 */
?>

<!-- JavaScript plugins comes here -->
<script src="/js/jquery.min.js"></script>        <!-- jQuery -->
<script src="/js/jquery.easing.min.js"></script>       <!-- jQuery easing -->
<script src="/js/jquery.scrollTo.min.js"></script>         <!-- Scroll to -->
<script src="/js/jquery.cycle.all.min.js"></script>    <!-- jQuery cycle -->
<script src="/js/jquery.form.min.js"></script>              <!-- jQuery form -->
<script src="/js/classie.min.js"></script>                 <!-- Class helper function -->
<script src="/js/retina.min.js"></script>                  <!-- Retina.js - support for HiDPI Displays -->
<script src="/js/waypoints.min.js"></script>           <!-- Waypoints -->
<script src="/js/nivo-lightbox.min.js"></script>       <!-- Lightbox/Modalbox -->
<script src="/js/jquery.fitvids.min.js"></script>          <!-- Responsive Video -->
<script src="/js/owl.carousel.min.js"></script>          <!-- Owl Carousel -->
<script src="/js/fastclick.min.js"></script>              <!-- FastClick -->
<script src="/js/main.min.js"></script>                    <!-- Main Js file -->

<!-- MOBILE NAVIGATION -->
<script>

    var showRight_1 = document.getElementById("showRight_1"),
        showRight_2 = document.getElementById("showRight_2"),
        hideRight = document.getElementById('hideRight'),
        menuRight = document.getElementById('cbp-spmenu-s2'),
        body = document.body;
    nav_open = false;

    showRight_1.onclick = function (event) {
        "use strict";
        event.preventDefault();
        classie.toggle(this, 'active');
        classie.toggle(menuRight, 'cbp-spmenu-open');
        nav_open = true;
        return false;
    };

    showRight_2.onclick = function (event) {
        "use strict";
        event.preventDefault();
        classie.toggle(this, 'active');
        classie.toggle(menuRight, 'cbp-spmenu-open');
        nav_open = true;
        return false;
    };

    //Hiding nav after clicking onto menu element
    menuRight.onclick = function (event) {
        "use strict";
        classie.toggle(this, 'active');
        classie.toggle(menuRight, 'cbp-spmenu-open');
        nav_open = false;
    };

    var hasParent = function (el, id) {
        if (el) {
            do {
                if (el.id === id) {
                    return true;
                }
                if (el.nodeType === 9) {
                    break;
                }
            }
            while ((el = el.parentNode));
        }
        return false;
    };

    document.addEventListener('click', function (e) {
        if (nav_open && !hasParent(e.target, 'cbp-spmenu-s2')) {
            e.preventDefault();
            classie.remove(menuRight, 'cbp-spmenu-open');
            nav_open = false;
        }
    }, true);

    document.addEventListener('touchend', function (e) {
        if (nav_open && !hasParent(e.target, 'cbp-spmenu-s2')) {
            e.preventDefault();
            classie.remove(menuRight, 'cbp-spmenu-open');
            nav_open = false;
            return false;
        }
    }, true);

</script>

<!-- Nav Bar show/hide after scrolling -->
<script>

    var $head = $('#menu_bar');

    $('.menu_bar-waypoint').each(
        function (i) {

            var $el = $(this),
                animClassDown = $el.data('animateDown'),
                animClassUp = $el.data('animateUp');

            $el.waypoint(
                function (direction) {
                    if (direction === 'down' && animClassDown) {
                        $head.attr('class', 'menu_bar mdl-color--primary ' + animClassDown);
                    }
                    else if (direction === 'up' && animClassUp) {
                        $head.attr('class', 'menu_bar mdl-color--primary ' + animClassUp);
                    }
                }, {
                    offset: function () {
                        navbarheight = $("#menu_bar").outerHeight() + 1;
                        return navbarheight;
                    }
                }
            );
        }
    );

</script>

<!-- Preloader -->
<script type="text/javascript">

    $(document).on(
        'ready',
        function () {
            $('#request-send-btn').on(
                'click',
                function () {
                    loaderShow();
                }
            );
        }
    );

    function loaderShow() {
        $('#preloader').addClass('transpared');
        $('#status').fadeIn();
        $('#preloader').fadeIn();
    }

    function loaderHide() {
        $('#status').fadeOut();
        $('#preloader').delay(350).fadeOut(300);
    }

    $(window).load(
        function () { // makes sure the whole site is loaded
            $('#status').fadeOut(); // will first fade out the loading animation
            $('#preloader').delay(350).fadeOut(300); // will fade out the white DIV that covers the website.
            $('body').delay(350).css({'overflow': 'visible'});
        }
    );

</script>

<noscript>
    <div><img src="https://mc.yandex.ru/watch/36219955" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
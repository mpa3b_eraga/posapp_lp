<?php
/**
 * Created by PhpStorm.
 * User: mpa3b
 * Date: 28.04.2016
 * Time: 10:26
 */
?>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

<link href="favicon.ico" rel="icon">

<!-- Material Design Lite -->
<link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.indigo-red.min.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<script src="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.min.js"></script>

<!-- Google Fonts -->
<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:300' rel='stylesheet' type='text/css'>

<!-- CSS Files comes here -->
<link href="/css/grid12.min.css" rel="stylesheet" media="screen">
<!-- Grid System -->
<link href="/css/james_typography.min.css" rel="stylesheet" media="screen">
<!-- Typography -->
<link href="/css/main.min.css" rel="stylesheet" media="screen">
<!-- Main CSS file -->
<link href="/css/color_scheme_light.min.css" rel="stylesheet" media="screen">
<!-- Color_scheme -->
<link href="/css/responsive.min.css" rel="stylesheet" media="screen">
<!-- Responsive Fixes -->
<link href="/css/animate.min.css" rel="stylesheet" media="screen">
<!-- Animate - animations -->
<link href="/css/nivo_lightbox.min.css" rel="stylesheet" media="screen">
<!-- Lightbox Styles -->
<link href="/css/nivo_lightbox_themes/default/default.min.css" rel="stylesheet" media="screen">
<!-- Lightbox Styles -->
<link href="/css/owl.carousel.min.css" rel="stylesheet" media="screen">
<!-- Owl - Carousel -->
<link href="/css/owl.theme.min.css" rel="stylesheet" media="screen">
<!-- Owl - Carousel -->

<!-- Modernizer and IE specyfic files -->
<script src="/js/modernizr.custom.min.js"></script>

<!-- Some fixes for old browsers -->
<!--[if IE 9] -->
<link href="/css/ie.min.css" rel="stylesheet" media="screen">
<!-- [endif]-->

<!-- Yandex.Metrika counter -->
<script src="https://mc.yandex.ru/metrika/watch.js" type="text/javascript"></script>
<script type="text/javascript"> try {
        var yaCounter36219955 = new Ya.Metrika({
            id: 36219955,
            clickmap: true,
            trackLinks: true,
            accurateTrackBounce: true,
            webvisor: true
        });
    } catch (e) {
    }
</script>

<!-- Яндекс.Поделиться -->
<script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
<script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>

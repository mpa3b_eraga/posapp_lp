<?php
// phpinfo();

$iserrormessage = 'Ошибка!';
$thanks = "Спасибо! Мы с Вами свяжемся!";
$alert = '';
$emptyname = 'Пожалуйста, укажите ваше имя.';
$emptyemail = 'Неверный адрес электонной почты.';
$emptycompany = 'А как называется ваша организация?';
$emptybranch = 'Чем занимается ваша компания?';
$emptyphone = 'Ваш номер телефона?';

$alertname = 'Неверный формат имени, пожалуйста, используйте только буквы.';
$alertemail = 'Неверный формат адреса электронной почты: yourname@domain.com';
$alertmessage = "Пожалуйста, используйте только буквы.";
$captcha = 'Ошибка спам-защиты! Вы точно не робот?';


$iserror = 0;

// recaptcha

if (empty($_REQUEST['g-recaptcha-response'])) {

    $iserror = 1;
    $alert .= "<li><p>" . $captcha . "</p></li>";

} else {

    $request = curl_init();

    curl_setopt_array(
        $request,
        array(
            CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => http_build_query(
                array(
                    'secret' => '6Ld1qR0TAAAAANkAsjrBVHAiIKU1r2YubKvky6dv',
                    'response' => $_REQUEST['g-recaptcha-response']
                )
            )
        )
    );

    $result = json_decode(curl_exec($request, true));

    curl_close($request);

    if ($result['success'] == 'false') {
        $iserror = 1;
        $alert .= "<li><p>" . $captcha . "</p></li>";
    }

}

//validate form-----------------------------------------------------------------

if (empty($_REQUEST['contact-name']) || $_REQUEST['contact-name'] == "") {
    $iserror = 1;
    $alert .= "<li><p>" . $emptyname . "</p></li>";
} elseif (preg_match("/[][{}()*+?.\\^$|]/i", $_REQUEST['contact-name'])) {
    $iserror = 1;
    $alert .= "<li><p>" . $alertname . "</p></li>";
}

if (empty($_REQUEST['contact-email']) || $_REQUEST['contact-email'] == "Enter your e-mail address") {
    $iserror = 1;
    $alert .= "<li><p>" . $emptyemail . "</p></li>";
} elseif (!preg_match("/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$/i", $_REQUEST['contact-email'])) {
    $iserror = 1;
    $alert .= "<li><p>" . $alertemail . "</p></li>";
}

if (empty($_REQUEST['contact-phone']) || $_REQUEST['contact-phone'] == "Ваш номер телефона?") {
    $iserror = 1;
    $alert .= "<li><p>" . $emptyphone . "</p></li>";
}

if (empty($_REQUEST['contact-company']) || $_REQUEST['contact-company'] == "Название вашей компании?") {
    $iserror = 1;
    $alert .= "<li><p>" . $emptycompany . "</p></li>";
}

if (empty($_REQUEST['contact-branch']) || $_REQUEST['contact-branch'] == "Чем занимается ваша компания?") {
    $iserror = 1;
    $alert .= "<li><p>" . $emptybranch . "</p></li>";
}

if ($iserror == 1) {
    echo "<script>
		loaderHide();
		$(\"#message\").addClass(\"warning\").stop().slideDown(\"normal\").fadeIn(\"normal\");
	 </script>";
    echo "<div class=\"alert alert-block alert-danger\">";
    echo "<div class=\"alert_title\"><p><strong>" . $iserrormessage . "</strong></p></div>";
    echo "<ul class=\"unordered\">";
    echo $alert;
    echo "</ul>";
    echo "</div>";
    die();
}

//sending request---------------------------------------------------------------

$url = "https://office.posapp.me/api/v1.0/test-request";

$ch = curl_init();

// set url
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, "email=" . $_POST['contact-email'] .
    '&company_name=' . $_POST['contact-company'] .
    '&sender_name=' . $_POST['contact-name'] .
    '&phone=' . $_POST['contact-phone'] .
    '&branch=' . $_POST['contact-branch']);

// $output contains the output string
$output = json_decode(curl_exec($ch));
// close curl resource to free up system resources
curl_close($ch);

//parse callback----------------------------------------------------------------

if ($output->success == 'true') {
    echo "<script>
		loaderHide();
		$(\"#message\").addClass(\"success\").stop().slideDown(\"normal\").fadeIn(\"normal\");
		yaCounter36219955.reachGoal('registrationFinished');
	</script>";
    echo "<div class=\"alert alert-block alert-success\">";
    echo "<div class=\"alert_title\"><p><strong>" . $thanks . "</strong></p></div>";
    echo "</div>";
    echo "<script>loaderHide();</script>";
} elseif ($output->success == 'false') {
    echo "<script>
        loaderHide();
		$(\"#message\").addClass(\"warning\").stop().slideDown(\"normal\").fadeIn(\"normal\");
	</script>";
    echo "<div class=\"alert alert-block alert-danger\">";
    echo "<div class=\"alert_title\"><p><strong>" . $iserrormessage . "</strong></p></div>";
    echo "<ul class=\"unordered\">";
    echo $alert;
    echo "</ul>";
    echo "</div>";
}

/**
 * Created by mpa3b on 21.04.2016.
 */

(function ($) {

    $('.toggle', '#questions-list').on(
        'click',
        function (event) {

            event.preventDefault();

            if ($(this).closest('.question-group').find('ul').hasClass('collapsed')) {

                $('.question-group', '#questions-list')
                    .removeClass('open')
                    .addClass('closed');

                $('ul.expanded', '.question-group', '#questions-list')
                    .slideUp()
                    .removeClass('expanded')
                    .addClass('collapsed');

                $(this).closest('.question-group')
                    .removeClass('closed')
                    .addClass('open')
                    .find('ul')
                    .slideToggle()
                    .removeClass('collapsed')
                    .addClass('expanded');

            }

            else {

                $(this).closest('.question-group')
                    .removeClass('open')
                    .addClass('closed')
                    .find('ul')
                    .slideToggle()
                    .removeClass('expanded')
                    .addClass('collapsed');

            }

        }
    );

    $('a[href^="#"]').on(
        'click',
        function (event) {

            event.preventDefault();

            var href = $(this).attr("href"),
                offsetTop = href === "#" ? 0 : $(href).offset().top - navbarheight + 2;

            $('html, body').stop().animate(
                {
                    scrollTop: offsetTop
                },
                1000,
                'easeInOutExpo'
            );

        }
    );



})(jQuery);
